#
# Copyright (c) 2018-2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'lib/vagrant-lxd'
require 'lib/vagrant-lxd/driver'
require 'lib/vagrant-lxd/synced_folder'
require 'lib/vagrant-lxd/version'

describe VagrantLXD::SyncedFolder do
  let(:driver) { double(:lxd) }
  let(:machine) { double(:machine).as_null_object }
  let(:hostpath) { '/foo' }
  let(:guestpath) { '/bar' }

  before do
    VagrantLXD::Driver.stub(:new).and_return(driver)
  end

  before do
    machine.stub(:provider_name).and_return(:lxd)
    driver.stub(:synced_folders_usable?).and_return(true)
    driver.stub(:mounted?).and_return(false)
  end

  context 'with a valid config' do
    let(:folders) do
      {
        guestpath => {
          hostpath: hostpath,
          guestpath: guestpath,
          config: {
            recursive: true,
            size: '40 GB',
          }
        }
      }
    end

    it 'should mount' do
      driver.should receive(:mount).with(guestpath, folders[guestpath])
      subject.enable(machine, folders, {})
    end
  end

  context 'with an invalid config' do
    let(:folders) do
      {
        guestpath => {
          hostpath: hostpath,
          guestpath: guestpath,
          config: {
            recursive: 1,
            size: [ '40 GB' ],
          }
        }
      }
    end

    it 'should indicate an error' do
      driver.should_not receive(:mount)
      proc { subject.enable(machine, folders, {}) }.should raise_error do |error|
        error.should be_a Vagrant::Errors::ConfigInvalid
        error.message.should include %{Invalid synced_folder `config' (hash values must be strings)}
      end
    end
  end
end
