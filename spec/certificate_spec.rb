#
# Copyright (c) 2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'openssl'
require 'webrick/https'

require 'fakefs/spec_helpers'

require 'lib/vagrant-lxd/version'
require 'lib/vagrant-lxd/driver/certificate'

describe VagrantLXD::Driver::Certificate do
  include FakeFS::SpecHelpers

  let(:paths) do
    ['/foo', '/bar', '/baz'].map { |x| Pathname.new(x) }
  end

  let(:path) { paths[1] }

  let(:client_crt) { path / 'client.crt' }
  let(:client_key) { path / 'client.key' }

  before do
    FileUtils.mkdir_p(paths)
  end

  subject do
    described_class
  end

  describe 'issuer_name' do
    subject { described_class.issuer_name }

    it { should be_a String }

    it 'should be a valid certificate name' do
      proc { OpenSSL::X509::Name.parse(subject) }.should_not raise_error
    end

    it { should include `whoami`.strip }
    it { should include `hostname`.strip }
    it { should include VagrantLXD::Version::DESCRIPTION }
    it { should include VagrantLXD::Version::VERSION }
  end

  describe 'generate' do
    subject { described_class.generate(path) }

    it { should be_a described_class }

    its(:certificate) { should eq client_crt }
    its(:key) { should eq client_key }

    it 'should create a valid certificate' do
      cert = proc { OpenSSL::X509::Certificate.new(File.read(subject.certificate)) }
      cert.should_not raise_error
    end

    it 'should create a valid key' do
      pkey = proc { OpenSSL::PKey.read(File.read(subject.key)) }
      pkey.should_not raise_error
    end
  end

  describe 'locate' do
    subject { described_class.locate(paths) }

    context 'with no certificates' do
      it { should be nil }
    end

    context 'with an invalid certificate' do
      before do
        File.write(client_crt, 'no')
        File.write(client_key, 'dice')
      end

      it { should be nil }
    end

    context 'with a valid certificate' do
      before do
        crt, key = silently { WEBrick::Utils.create_self_signed_cert(1024, [], '') }
        File.write(client_crt, crt.to_s)
        File.write(client_key, key.to_s)
      end

      it { should be_a described_class }

      its(:certificate) { should eq client_crt }
      its(:key) { should eq client_key }
    end

    context 'with a generated certificate' do
      before do
        described_class.generate(path)
      end

      it { should be_a described_class }

      its(:certificate) { should eq client_crt }
      its(:key) { should eq client_key }
    end
  end
end
