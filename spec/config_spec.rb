#
# Copyright (c) 2018-2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'lib/vagrant-lxd'
require 'lib/vagrant-lxd/config'
require 'lib/vagrant-lxd/version'

describe VagrantLXD::Config do
  let('t') { double('t') }
  let('machine') { double('machine') }

  let('validation_errors') do
    subject.validate(machine).fetch(VagrantLXD::Version::NAME)
  end

  context 'with default settings' do
    subject do
      super().tap(&:finalize!)
    end

    it('should be valid') { validation_errors.should eq [] }
    its('name') { should be nil }
    its('api_endpoint') { should eq URI("https://127.0.0.1:8443") }
    its('timeout') { should be 10 }
    its('environment') { should == {} }
    its('ephemeral') { should be false }
    its('nesting') { should be nil }
    its('privileged') { should be nil }
    its('profiles') { should eq ['default'] }
    its('client_certificate') { should be nil }
    its('client_key') { should be nil }
    its('vagrant_uid') { should be 1000 }
    its('vagrant_gid') { should be 1000 }
  end

  context 'with an unrecognised setting' do
    it 'should indicate an error' do
      subject.nonexistent_setting = true
      subject.finalize!
      I18n.should_receive('t').with('vagrant.config.common.bad_field', fields: 'nonexistent_setting').and_return(t)
      validation_errors.should eq [t]
    end
  end

  describe 'the profiles setting' do
    it 'should accept valid values' do
      value = ['crayola']
      subject.profiles = value
      subject.finalize!
      subject.profiles.should eq value
      validation_errors.should eq []
    end

    it 'should reject non-array values' do
      subject.profiles = 36
      subject.finalize!
      validation_errors.should eq ["Invalid `profiles' (value must be an array of strings): 36"]
    end

    it 'should reject invalid profile names' do
      profiles = [36, 'chambers']
      subject.profiles = profiles
      subject.finalize!
      validation_errors.should eq [%{Invalid `profiles' (value must be an array of strings): #{profiles}}]
    end
  end

  %w(devices environment).each do |setting|
    describe "the #{setting} setting" do
      it 'should reject non-hash values' do
        value = ['enter', 'the', 'dragon']
        subject.send(:"#{setting}=", value)
        subject.finalize!
        validation_errors.should eq [%{Invalid `#{setting}' (value must be a hash): #{value}}]
      end

      it 'should reject invalid hash keys' do
        value = {123 => 'abc'}
        subject.send(:"#{setting}=", value)
        subject.finalize!
        validation_errors.should eq [%{Invalid `#{setting}' (keys must be strings or symbols): #{value}}]
      end
    end
  end

  describe 'the devices setting' do
    it 'should reject invalid hash values' do
      value = {'abc' => '123'}
      subject.devices = value
      subject.finalize!
      validation_errors.should eq [%{Invalid `devices' (values must be hashes): #{value}}]
    end

    it 'should accept valid keys' do
      value = {'abc' => {}, :'123' => {}}
      subject.devices = value
      subject.finalize!
      subject.devices.should eq value
      validation_errors.should eq []
    end
  end

  describe 'the environment setting' do
    it 'should reject invalid hash values' do
      value = {'abc' => 123}
      subject.environment = value
      subject.finalize!
      validation_errors.should eq [%{Invalid `environment' (values must be strings): #{value}}]
    end

    it 'should accept valid keys' do
      value = {'abc' => '123', :xyz => '789'}
      subject.environment = value
      subject.finalize!
      subject.environment.should eq value
      validation_errors.should eq []
    end
  end

  describe 'the config setting' do
    it 'should reject non-hash values' do
      value = 0x13EA57_1110DE
      subject.config = value
      subject.finalize!
      validation_errors.should eq ["Invalid `config' (value must be a hash): #{value}"]
    end

    it 'should reject invalid hash keys' do
      value = {'environment.MODE' => 'BEAST'}
      subject.config = value
      subject.finalize!
      validation_errors.should eq [%{Invalid `config' (keys must be symbols): #{value}}]
    end

    it 'should accept valid keys' do
      value = {'environment.MODE': 'BEAST'}
      subject.config = value
      subject.finalize!
      subject.config.should eq value
      validation_errors.should eq []
    end
  end

  describe 'the client_certificate and client_key settings' do
    it 'should reject invalid values' do
      subject.client_certificate = "/nonexistent/file"
      subject.client_key = true
      subject.finalize!
      validation_errors.should match_array [
        %r{Invalid `client_certificate' \(unable to read certificate\):},
         %{Invalid `client_key' (value must be a string): true},
      ]
    end

    it 'should require both values to be set' do
      subject.client_certificate = '/some/file'
      subject.client_key = nil
      subject.finalize!
      validation_errors.should include "You must provide both `client_certificate' and `client_key'"

      subject.client_certificate = nil
      subject.client_key = '/some/file'
      subject.finalize!
      validation_errors.should include "You must provide both `client_certificate' and `client_key'"
    end
  end

  describe 'the uid and gid settings' do
    it 'should reject invalid values' do
      subject.vagrant_uid = "foo"
      subject.vagrant_gid = -1
      subject.finalize!
      validation_errors.should match_array [
        %{Invalid `vagrant_uid' (value must be a non-negative integer): "foo"},
        %{Invalid `vagrant_gid' (value must be a non-negative integer): -1},
      ]
    end

    it 'should default gid to uid' do
      subject.vagrant_uid = 500
      subject.finalize!
      subject.vagrant_uid.should eq 500
      subject.vagrant_gid.should eq 500
      validation_errors.should eq []
    end

    it 'should allow different values to be specified' do
      subject.vagrant_uid = 500
      subject.vagrant_gid = 501
      subject.finalize!
      subject.vagrant_uid.should eq 500
      subject.vagrant_gid.should eq 501
      validation_errors.should eq []
    end
  end
end
